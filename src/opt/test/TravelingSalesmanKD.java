package opt.test;

import java.util.Arrays;
import java.util.Random;

import dist.DiscreteDependencyTree;
import dist.DiscretePermutationDistribution;
import dist.DiscreteUniformDistribution;
import dist.Distribution;
import java.util.HashMap;
import java.util.Map;

import opt.SwapNeighbor;
import opt.GenericHillClimbingProblem;
import opt.HillClimbingProblem;
import opt.NeighborFunction;
import opt.RandomizedHillClimbing;
import opt.SimulatedAnnealing;
import opt.example.*;
import opt.ga.CrossoverFunction;
import opt.ga.SwapMutation;
import opt.ga.GenericGeneticAlgorithmProblem;
import opt.ga.GeneticAlgorithmProblem;
import opt.ga.MutationFunction;
import opt.ga.StandardGeneticAlgorithm;
import opt.prob.GenericProbabilisticOptimizationProblem;
import opt.prob.MIMIC;
import opt.prob.ProbabilisticOptimizationProblem;
import shared.FixedIterationTrainer;

/**
 *
 * @author Andrew Guillory gtg008g@mail.gatech.edu
 * @version 1.0
 */
public class TravelingSalesmanKD {

    /**
     * The n value
     */
    private static int N = 50;

    private static final int ITERATIONS = 2500;

    /**
     * The test main
     *
     * @param args ignored
     */
    public static void main(String[] args) {
        Random random = new Random();
        Runtime runtime = Runtime.getRuntime();
        System.out.println("Optimizer\tIterations\t N \t time \t Optimal \t Memory");
        int[] nodes = {1, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
        //int[] nodes = {50};
        int[] iterations = {500};
        boolean show = true;
        for (int n = 0; n < nodes.length; n++) {
            N = nodes[n];
            // create the random points
            double[][] points = new double[N][2];
            for (int i = 0; i < points.length; i++) {
                points[i][0] = random.nextDouble();
                points[i][1] = random.nextDouble();
            }

            
            //int[] iterations = {200};
            for (int x = 0; x < iterations.length; x++) {
                HashMap<String, Double> trainingTimes = new HashMap<>();
                int trainingIterations = iterations[x];

                // for rhc, sa, and ga we use a permutation based encoding
                TravelingSalesmanEvaluationFunction ef = new TravelingSalesmanRouteEvaluationFunction(points);
                Distribution odd = new DiscretePermutationDistribution(N);
                NeighborFunction nf = new SwapNeighbor();
                MutationFunction mf = new SwapMutation();
                CrossoverFunction cf = new TravelingSalesmanCrossOver(ef);
                HillClimbingProblem hcp = new GenericHillClimbingProblem(ef, odd, nf);
                GeneticAlgorithmProblem gap = new GenericGeneticAlgorithmProblem(ef, odd, mf, cf);

                //System.out.println("Iterations: " + ITERATIONS);
                runtime.gc();
                double start = System.nanoTime(), end, trainingTime;
//            //System.out.println("RHC");
                RandomizedHillClimbing rhc = new RandomizedHillClimbing(hcp);
                FixedIterationTrainer fit = new FixedIterationTrainer(rhc, ITERATIONS);
                fit.train(show);
                //System.out.println(ef.value(rhc.getOptimal()));
                end = System.nanoTime();
                trainingTime = end - start;
                trainingTime /= Math.pow(10, 9);
                //trainingTimes.put("RHC", trainingTime);
                System.out.println("RHC\t" + trainingIterations + "\t" + N + "\t" + trainingTime + "\t" + ef.value(rhc.getOptimal()) + "\t" + (runtime.totalMemory() - runtime.freeMemory()));

                runtime.gc();
                start = System.nanoTime();
                //System.out.println("SA");
                SimulatedAnnealing sa = new SimulatedAnnealing(1E12, .1, hcp);
                fit = new FixedIterationTrainer(sa, ITERATIONS);
                fit.train(show);
                //System.out.println(ef.value(sa.getOptimal()));
                end = System.nanoTime();
                trainingTime = end - start;
                trainingTime /= Math.pow(10, 9);
                //trainingTimes.put("SA", trainingTime);
                System.out.println("SA\t" + trainingIterations + "\t"  + N + "\t" + trainingTime + "\t" + ef.value(sa.getOptimal()) + "\t" + (runtime.totalMemory() - runtime.freeMemory()));

                runtime.gc();
                start = System.nanoTime();
                //System.out.println("GA");
                StandardGeneticAlgorithm ga = new StandardGeneticAlgorithm(200, 150, 20, gap);
                fit = new FixedIterationTrainer(ga, ITERATIONS);
                fit.train(show);
                //System.out.println(ef.value(ga.getOptimal()));
                end = System.nanoTime();
                trainingTime = end - start;
                trainingTime /= Math.pow(10, 9);
                //trainingTimes.put("GA", trainingTime);
                System.out.println("GA\t" + trainingIterations + "\t" + N + "\t" + trainingTime + "\t" + ef.value(ga.getOptimal()) + "\t" + (runtime.totalMemory() - runtime.freeMemory()));

                // for mimic we use a sort encoding
                ef = new TravelingSalesmanSortEvaluationFunction(points);
                int[] ranges = new int[N];
                Arrays.fill(ranges, N);
                odd = new DiscreteUniformDistribution(ranges);
                Distribution df = new DiscreteDependencyTree(.1, ranges);
                ProbabilisticOptimizationProblem pop = new GenericProbabilisticOptimizationProblem(ef, odd, df);

                runtime.gc();
                start = System.nanoTime();
                //System.out.println("MIMIC");
                MIMIC mimic = new MIMIC(200, 100, pop);
                fit = new FixedIterationTrainer(mimic, ITERATIONS);
                fit.train(show);
                //System.out.println(ef.value(mimic.getOptimal()));
                end = System.nanoTime();
                trainingTime = end - start;
                trainingTime /= Math.pow(10, 9);
                //trainingTimes.put("MIMIC", trainingTime);
                System.out.println("MIMIC\t" + trainingIterations + "\t" + N + "\t" + trainingTime + "\t" + ef.value(mimic.getOptimal()) + "\t" + (runtime.totalMemory() - runtime.freeMemory()));

//        System.out.println("\nTraining Times");
//        for(Map.Entry v:trainingTimes.entrySet()) {
//            System.out.println(v.getKey() + " " + v.getValue());
//        }
            }
        }
    }
}
