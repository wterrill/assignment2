package opt.test;

import java.util.ArrayList;
import java.util.Random;
import java.util.Arrays;

import dist.DiscreteDependencyTree;
import dist.DiscreteUniformDistribution;
import dist.Distribution;
import java.util.HashMap;

import opt.DiscreteChangeOneNeighbor;
import opt.EvaluationFunction;
import opt.GenericHillClimbingProblem;
import opt.HillClimbingProblem;
import opt.NeighborFunction;
import opt.RandomizedHillClimbing;
import opt.SimulatedAnnealing;
import opt.example.*;
import opt.ga.CrossoverFunction;
import opt.ga.DiscreteChangeOneMutation;
import opt.ga.GenericGeneticAlgorithmProblem;
import opt.ga.GeneticAlgorithmProblem;
import opt.ga.MutationFunction;
import opt.ga.StandardGeneticAlgorithm;
import opt.ga.UniformCrossOver;
import opt.prob.GenericProbabilisticOptimizationProblem;
import opt.prob.MIMIC;
import opt.prob.ProbabilisticOptimizationProblem;
import shared.FixedIterationTrainer;
import java.util.List;
import java.util.Map;

/**
 * A test of the knap sack problem
 * @author Andrew Guillory gtg008g@mail.gatech.edu
 * @version 1.0
 */
public class KnapsackANALYSIS {
    /** Random number generator */
    //private static final Random random = new Random();
    /** The number of items */
    //private static final int NUM_ITEMS = 2;
    /** The number of copies each */
    //private static final int COPIES_EACH = 3;
    /** The maximum weight for a single element */
    //private static final double MAX_WEIGHT = 50;
    /** The maximum volume for a single element */
    //private static final double MAX_VOLUME = 50;
    /** The volume of the knapsack */
    //private static final double MAX_KNAPSACK_VOLUME = 6.5; //I put the 5 there and got rid of the other stuff.  this seems to set the upper limit
         //MAX_VOLUME * NUM_ITEMS * COPIES_EACH * .4; 
    /**
     * The test main
     * @param args ignored
     */
    public static ArrayList<Double> main(int NUM_ITEMS, int COPIES_EACH, double MAX_KNAPSACK_VOLUME, double[] weights, double[] volumes, int rhcIter, int saIter, int gaIter, int mimicIter ) {

        ArrayList<Double> answers = new ArrayList<Double>();
        int[] copies = new int[NUM_ITEMS];
        Arrays.fill(copies, COPIES_EACH);
        //double[] weights = new double[NUM_ITEMS];
        //double[] volumes = new double[NUM_ITEMS];
        //for (int i = 0; i < NUM_ITEMS; i++) {
        //    weights[i] = random.nextDouble() * MAX_WEIGHT; //i+1;
        //    volumes[i] = random.nextDouble() * MAX_VOLUME; //i*2;
        //}
        //put something that should be picked. The weight is heavy and the volume exactly fits the knapsack
        //weights[NUM_ITEMS-1] = 4500;
        //volumes[NUM_ITEMS-1] = 6.5;
        int[] inventory = new int[NUM_ITEMS];
        Arrays.fill(inventory, COPIES_EACH + 1);
        EvaluationFunction ef = new KnapsackEvaluationFunction(weights, volumes, MAX_KNAPSACK_VOLUME, copies);
        Distribution odd = new DiscreteUniformDistribution(inventory);
        NeighborFunction nf = new DiscreteChangeOneNeighbor(inventory);
        MutationFunction mf = new DiscreteChangeOneMutation(inventory);
        CrossoverFunction cf = new UniformCrossOver();
        Distribution df = new DiscreteDependencyTree(.1, inventory); 
        HillClimbingProblem hcp = new GenericHillClimbingProblem(ef, odd, nf);
        GeneticAlgorithmProblem gap = new GenericGeneticAlgorithmProblem(ef, odd, mf, cf);
        ProbabilisticOptimizationProblem pop = new GenericProbabilisticOptimizationProblem(ef, odd, df);
        FixedIterationTrainer fit;
        HashMap<String,Double> trainingTimes = new HashMap();
        //double start;
        
        double start = System.nanoTime(), end, trainingTime;
        if (rhcIter > 0)  {
            
            //System.out.println("RHC");
            RandomizedHillClimbing rhc = new RandomizedHillClimbing(hcp);      
            fit = new FixedIterationTrainer(rhc, rhcIter);
            fit.train();
            //System.out.println(ef.value(rhc.getOptimal())); //this is pretty weak. getOptimal just returns the current value, and ef.value scores it again.
            answers.add(ef.value(rhc.getOptimal()));
            end = System.nanoTime();
            trainingTime = end - start;
            trainingTime /= Math.pow(10, 9);
            trainingTimes.put("RHC", trainingTime/rhcIter);
        }
        if (saIter > 0)  {
            start = System.nanoTime();
            //System.out.println("SA");
            SimulatedAnnealing sa = new SimulatedAnnealing(100, .95, hcp);
            fit = new FixedIterationTrainer(sa, saIter);
            fit.train();
            //System.out.println(ef.value(sa.getOptimal()));
            answers.add(ef.value(sa.getOptimal()));
            end = System.nanoTime();
            trainingTime = end - start;
            trainingTime /= Math.pow(10, 9);
            trainingTimes.put("GA", trainingTime/saIter);
        }
        if (gaIter > 0)  {
            start = System.nanoTime();
            //System.out.println("GA");
            StandardGeneticAlgorithm ga = new StandardGeneticAlgorithm(200, 150, 25, gap);
            fit = new FixedIterationTrainer(ga, gaIter);
            fit.train();
            //System.out.println(ef.value(ga.getOptimal()));
            answers.add(ef.value(ga.getOptimal()));
            end = System.nanoTime();
            trainingTime = end - start;
            trainingTime /= Math.pow(10, 9);
            trainingTimes.put("GA", trainingTime/gaIter);
        }
        if (mimicIter > 0)  {
            start = System.nanoTime();
            //System.out.println("MIMIC");
            MIMIC mimic = new MIMIC(200, 100, pop);
            fit = new FixedIterationTrainer(mimic, mimicIter);
            fit.train();
            //System.out.println(ef.value(mimic.getOptimal()));
            answers.add(ef.value(mimic.getOptimal())); 
            end = System.nanoTime();
            trainingTime = end - start;
            trainingTime /= Math.pow(10, 9);
            trainingTimes.put("MIMIC", trainingTime/mimicIter);

        }
        System.out.println("\nTraining Times");
        for(Map.Entry v:trainingTimes.entrySet()) {
            System.out.println(v.getKey() + "\t" + v.getValue());
        }
        return answers;
    }

}
