package opt.test;

import java.util.Arrays;
import java.util.Random;

import dist.DiscreteDependencyTree;
import dist.DiscreteUniformDistribution;
import dist.Distribution;
import java.util.HashMap;
import java.util.Map.Entry;

import opt.DiscreteChangeOneNeighbor;
import opt.EvaluationFunction;
import opt.GenericHillClimbingProblem;
import opt.HillClimbingProblem;
import opt.NeighborFunction;
import opt.RandomizedHillClimbing;
import opt.SimulatedAnnealing;
import opt.example.*;
import opt.ga.CrossoverFunction;
import opt.ga.DiscreteChangeOneMutation;
import opt.ga.GenericGeneticAlgorithmProblem;
import opt.ga.GeneticAlgorithmProblem;
import opt.ga.MutationFunction;
import opt.ga.StandardGeneticAlgorithm;
import opt.ga.UniformCrossOver;
import opt.prob.GenericProbabilisticOptimizationProblem;
import opt.prob.MIMIC;
import opt.prob.ProbabilisticOptimizationProblem;
import shared.FixedIterationTrainer;

/**
 * A test of the knap sack problem
 *
 * @author Andrew Guillory gtg008g@mail.gatech.edu
 * @version 1.0
 */
public class KnapsackKD {

    /**
     * Random number generator
     */
    private static final Random random = new Random();
    /**
     * The number of items
     */
    private static final int NUM_ITEMS = 40;
    /**
     * The number of copies each
     */
    private static final int COPIES_EACH = 4;
    /**
     * The maximum weight for a single element
     */
    private static final double MAX_WEIGHT = 50;
    /**
     * The maximum volume for a single element
     */
    private static final double MAX_VOLUME = 50;
    /**
     * The volume of the knapsack
     */
    private static final double KNAPSACK_VOLUME
            = MAX_VOLUME * NUM_ITEMS * COPIES_EACH * .4;

    private static final int ITERATIONS = 40000;

    /**
     * The test main
     *
     * @param args ignored
     */
    public static void main(String[] args) {
        //HashMap<String, Double> trainingTimes = new HashMap<>();
        //int[] iterations = {500, 1000, 2000, 3000, 4000, 5000, 10000, 20000, 30000, 40000, 50000};
        int[] iterations = {1000};
        for (int x = 0; x < iterations.length; x++) {
            int trainingIterations = iterations[x];
            boolean show = true;
            int[] copies = new int[NUM_ITEMS];
            Arrays.fill(copies, COPIES_EACH);
            double[] weights = new double[NUM_ITEMS];
            double[] volumes = new double[NUM_ITEMS];
            for (int i = 0; i < NUM_ITEMS; i++) {
                weights[i] = random.nextDouble() * MAX_WEIGHT;
                volumes[i] = random.nextDouble() * MAX_VOLUME;
            }
            int[] ranges = new int[NUM_ITEMS];
            Arrays.fill(ranges, COPIES_EACH + 1);
            EvaluationFunction ef = new KnapsackEvaluationFunction(weights, volumes, KNAPSACK_VOLUME, copies);
            Distribution odd = new DiscreteUniformDistribution(ranges);
            NeighborFunction nf = new DiscreteChangeOneNeighbor(ranges);
            MutationFunction mf = new DiscreteChangeOneMutation(ranges);
            CrossoverFunction cf = new UniformCrossOver();
            Distribution df = new DiscreteDependencyTree(.1, ranges);
            HillClimbingProblem hcp = new GenericHillClimbingProblem(ef, odd, nf);
            GeneticAlgorithmProblem gap = new GenericGeneticAlgorithmProblem(ef, odd, mf, cf);
            ProbabilisticOptimizationProblem pop = new GenericProbabilisticOptimizationProblem(ef, odd, df);

            //System.out.println("Iterations: " + trainingIterations);
            double start = System.nanoTime(), end, trainingTime;
            //System.out.println("RHC");
            RandomizedHillClimbing rhc = new RandomizedHillClimbing(hcp);
            FixedIterationTrainer fit = new FixedIterationTrainer(rhc, trainingIterations);
            fit.train(show);
            //System.out.println(ef.value(rhc.getOptimal()));
            end = System.nanoTime();
            trainingTime = end - start;
            trainingTime /= Math.pow(10, 9);
            //trainingTimes.put("RHC", trainingTime);
            System.out.println("RHC\t"+trainingIterations+"\t"+trainingTime+"\t"+ef.value(rhc.getOptimal()));

            start = System.nanoTime();
            //System.out.println("SA");
            SimulatedAnnealing sa = new SimulatedAnnealing(100, .1, hcp);
            fit = new FixedIterationTrainer(sa, trainingIterations);
            fit.train(show);
            //System.out.println(ef.value(sa.getOptimal()));
            end = System.nanoTime();
            trainingTime = end - start;
            trainingTime /= Math.pow(10, 9);
            //trainingTimes.put("SA", trainingTime);
            System.out.println("SA\t"+trainingIterations+"\t"+trainingTime+"\t"+ef.value(rhc.getOptimal()));

            start = System.nanoTime();
            //System.out.println("GA");
            StandardGeneticAlgorithm ga = new StandardGeneticAlgorithm(200, 150, 25, gap);
            fit = new FixedIterationTrainer(ga, trainingIterations);
            fit.train(show);
            //System.out.println(ef.value(ga.getOptimal()));
            end = System.nanoTime();
            trainingTime = end - start;
            trainingTime /= Math.pow(10, 9);
            //trainingTimes.put("GA", trainingTime);
            System.out.println("GA\t"+trainingIterations+"\t"+trainingTime+"\t"+ef.value(rhc.getOptimal()));

            start = System.nanoTime();
            //System.out.println("MIMIC");
            MIMIC mimic = new MIMIC(200, 100, pop);
            fit = new FixedIterationTrainer(mimic, trainingIterations);
            fit.train(show);
            //System.out.println(ef.value(mimic.getOptimal()));
            end = System.nanoTime();
            trainingTime = end - start;
            trainingTime /= Math.pow(10, 9);
            //trainingTimes.put("MIMIC", trainingTime);
            System.out.println("MIMIC\t"+trainingIterations+"\t"+trainingTime+"\t"+ef.value(rhc.getOptimal()));

//            System.out.println("\nTraining Times");
//            for (Entry v : trainingTimes.entrySet()) {
//                System.out.println(v.getKey() + "\t" + v.getValue());
//            }

        }

    }

}
