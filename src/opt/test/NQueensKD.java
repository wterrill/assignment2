package opt.test;

import java.util.Arrays;
import java.util.Random;
import opt.ga.NQueensFitnessFunction;
import dist.DiscreteDependencyTree;
import dist.DiscretePermutationDistribution;
import dist.DiscreteUniformDistribution;
import dist.Distribution;
import opt.DiscreteChangeOneNeighbor;
import opt.EvaluationFunction;
import opt.GenericHillClimbingProblem;
import opt.HillClimbingProblem;
import opt.NeighborFunction;
import opt.RandomizedHillClimbing;
import opt.SimulatedAnnealing;
import opt.SwapNeighbor;
import opt.example.*;
import opt.ga.CrossoverFunction;
import opt.ga.DiscreteChangeOneMutation;
import opt.ga.SingleCrossOver;
import opt.ga.GenericGeneticAlgorithmProblem;
import opt.ga.GeneticAlgorithmProblem;
import opt.ga.MutationFunction;
import opt.ga.StandardGeneticAlgorithm;
import opt.ga.SwapMutation;
import opt.prob.GenericProbabilisticOptimizationProblem;
import opt.prob.MIMIC;
import opt.prob.ProbabilisticOptimizationProblem;
import shared.FixedIterationTrainer;

/**
 * @author kmanda1
 * @version 1.0
 */
public class NQueensKD {

    /**
     * The n value
     */
    private static int N = 10;

    /**
     * The t value
     */
    public static void main(String[] args) {
        Runtime runtime = Runtime.getRuntime();
        int[] NQueens = {1,2,3,4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
        int[] iterations = {5000};
        //int[] iterations = {500,1000,2000,3000,4000,5000};
        System.out.println("Optimizer\tIterations\t N \t time \t Optimal \t Memory");
        for (int n = 0; n < NQueens.length; n++) {
            N = NQueens[n];
            int[] ranges = new int[N];
            Random random = new Random(N);
            for (int i = 0; i < N; i++) {
                ranges[i] = random.nextInt();
            }
            NQueensFitnessFunction ef = new NQueensFitnessFunction();
            Distribution odd = new DiscretePermutationDistribution(N);
            NeighborFunction nf = new SwapNeighbor();
            MutationFunction mf = new SwapMutation();
            CrossoverFunction cf = new SingleCrossOver();
            Distribution df = new DiscreteDependencyTree(.1);
            HillClimbingProblem hcp = new GenericHillClimbingProblem(ef, odd, nf);
            GeneticAlgorithmProblem gap = new GenericGeneticAlgorithmProblem(ef, odd, mf, cf);
            ProbabilisticOptimizationProblem pop = new GenericProbabilisticOptimizationProblem(ef, odd, df);

            for (int x = 0; x < iterations.length; x++) {
                int trainingIterations = iterations[x];
                boolean show = false;
                long starttime = System.currentTimeMillis();
                runtime.gc();
                RandomizedHillClimbing rhc = new RandomizedHillClimbing(hcp);
                FixedIterationTrainer fit = new FixedIterationTrainer(rhc, trainingIterations);
                fit.train(show);
                long trainingTime = System.currentTimeMillis() - starttime;
                System.out.println("RHC\t" + trainingIterations + "\t" + N + "\t" + trainingTime + "\t" + ef.value(rhc.getOptimal())+ "\t" + (runtime.totalMemory() - runtime.freeMemory()));
            //System.out.println("RHC: " + ef.value(rhc.getOptimal()));
                //System.out.println("RHC: Board Position: ");
                // System.out.println(ef.boardPositions());
                //System.out.println("Time : " + (System.currentTimeMillis() - starttime));

            //System.out.println("============================");
                starttime = System.currentTimeMillis();
                runtime.gc();
                SimulatedAnnealing sa = new SimulatedAnnealing(1E1, .1, hcp);
                fit = new FixedIterationTrainer(sa, trainingIterations);
                fit.train(show);
                trainingTime = System.currentTimeMillis() - starttime;
                System.out.println("SA\t" + trainingIterations + "\t" + N + "\t" + trainingTime + "\t" + ef.value(sa.getOptimal())+ "\t" + (runtime.totalMemory() - runtime.freeMemory()));
//            System.out.println("SA: " + ef.value(sa.getOptimal()));
//            System.out.println("SA: Board Position: ");
//            // System.out.println(ef.boardPositions());
//            System.out.println("Time : " + (System.currentTimeMillis() - starttime));
//
//            System.out.println("============================");

                starttime = System.currentTimeMillis();
                runtime.gc();
                StandardGeneticAlgorithm ga = new StandardGeneticAlgorithm(200, 0, 10, gap);
                fit = new FixedIterationTrainer(ga, trainingIterations);
                fit.train(show);
                trainingTime = System.currentTimeMillis() - starttime;
                System.out.println("GA\t" + trainingIterations + "\t" + N + "\t" +  trainingTime + "\t" + ef.value(ga.getOptimal())+ "\t" + (runtime.totalMemory() - runtime.freeMemory()));
//            System.out.println("GA: " + ef.value(ga.getOptimal()));
//            System.out.println("GA: Board Position: ");
//            //System.out.println(ef.boardPositions());
//            System.out.println("Time : " + (System.currentTimeMillis() - starttime));
//
//            System.out.println("============================");

                starttime = System.currentTimeMillis();
                runtime.gc();
                MIMIC mimic = new MIMIC(200, 10, pop);
                fit = new FixedIterationTrainer(mimic, trainingIterations);
                fit.train(show);
                trainingTime = System.currentTimeMillis() - starttime;
                System.out.println("MIMIC\t" + trainingIterations + "\t" + N + "\t" + trainingTime + "\t" + ef.value(mimic.getOptimal())+ "\t" + (runtime.totalMemory() - runtime.freeMemory()));
//            System.out.println("MIMIC: " + ef.value(mimic.getOptimal()));
//            System.out.println("MIMIC: Board Position: ");
//            //System.out.println(ef.boardPositions());
//            System.out.println("Time : " + (System.currentTimeMillis() - starttime));
            }
        }
    }
}
