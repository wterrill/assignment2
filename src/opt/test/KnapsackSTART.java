/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opt.test;

import java.util.Random;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author Engineer1
 */
public class KnapsackSTART {
    public static void main(String[] args) {
        // initialize data for Knapsack run int NUM_ITEMS = 40;
        int NUM_ITEMS = 40;
        int COPIES_EACH = 4;
        double MAX_KNAPSACK_VOLUME = 3200;
        double[] weights = new double[NUM_ITEMS];
        double[] volumes = new double[NUM_ITEMS];
        double MAX_WEIGHT = 50;
        double MAX_VOLUME = 50;
        
        Random random = new Random();
        for (int i = 0; i < NUM_ITEMS; i++) {
            weights[i] = random.nextDouble() * MAX_WEIGHT; //i+1;
            volumes[i] = random.nextDouble() * MAX_VOLUME; //i*2;
        }
        
        //initialize data for optimization iterations
        int rhcIter = 200000;
        int saIter = 200000;
        int gaIter = 1000;
        int mimicIter = 1000;
        
        //initialize information for this program
        List<Double> answers = Arrays.asList(1.0, 2.0, 3.0, 4.0);
        ArrayList<List<Double>> results = new ArrayList<List<Double>>();
        int runs = 1;
        //List results = new ArrayList<();
        for (int i = 0; i < runs; i++) {
        answers = KnapsackANALYSIS.main(NUM_ITEMS, COPIES_EACH, MAX_KNAPSACK_VOLUME, weights, volumes,  rhcIter,  saIter,  gaIter,  mimicIter);
        System.out.println(answers);
        results.add(answers);
        } 
        //print out results
        
        System.out.print("Run");
        System.out.print("\t");
        System.out.print("rhc");
        System.out.print("\t");
        System.out.print("sa");
        System.out.print("\t");
        System.out.print("ga");
        System.out.print("\t");
        System.out.println("mimic");
        for (int i = 0; i < runs; i++){
            System.out.print(i + 1);
            System.out.print("\t");
            System.out.print(results.get(i).get(0));
            System.out.print("\t");
            System.out.print(results.get(i).get(1));
            System.out.print("\t");
            System.out.print(results.get(i).get(2));
            System.out.print("\t");
            System.out.println(results.get(i).get(3));  
        }
    }

}
